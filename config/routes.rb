Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'api/list_catalog' => 'api#list_catalog'
  get 'api/list_product' => 'api#list_product'
  
  root to: "application#index"
end
