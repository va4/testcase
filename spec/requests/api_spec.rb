require 'rails_helper'

RSpec.describe "Api tests", type: :request do

  describe "GET /api/list_catalog" do
    it "should return list of catalogs" do
      10.times { |c| Catalog.create(name: "Test Calalog #{c}") }

      get api_list_catalog_path
      expect(response).to have_http_status(200)
      expect(json.last['id']).to eq Catalog.last.id
      expect(json.count).to eq Catalog.count 
    end
  end
  
  describe "GET /api/list_products" do
    it "should return list of products in catalog" do
      catalog = Catalog.create(name: "Test Calalog")
      10.times do |p|
        catalog.products.create!(name: "Test product #{p}", price: rand(0..9999))
      end

      get api_list_product_path({catalog_id: catalog.id})
      expect(response).to have_http_status(200)
      expect(json.count).to eq catalog.products.count
      expect(json.last['id']).to eq catalog.products.last.id 
    end
  end
  
end