# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

20.times do |c|
  catalog = Catalog.create!(name: "Catalog #{c}", description: "Test description #{c}")
  10.times do |p| 
    catalog.products.create!(name: "Test product #{p}", description: "Test description #{p}", price: rand(0..99999))
  end
end
