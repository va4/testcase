class ApiController < ApplicationController
  
  def list_catalog
    @catalogs = Catalog.all
    render json: @catalogs
  end
  
  def list_product
    @products = Catalog.find(params[:catalog_id]).products 
    render json: @products
  end

end
