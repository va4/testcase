ActiveAdmin.register Product do
  permit_params :name, :description, :price, :catalog_id
end