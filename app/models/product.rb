class Product < ActiveRecord::Base
  belongs_to :catalog
  
  validates :name,
            :length => { :minimum => 3, :maximum => 45, },
            :presence => true

 validates :catalog,
           :presence => true
                                 
 validates :price,
           :numericality => { 
             :greater_than_or_equal_to => 0,
             :less_than_or_equal_to => 99999.99 },
           :presence => true
end
